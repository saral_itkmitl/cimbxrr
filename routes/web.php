<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Carbon\Carbon;

Route::get('/test', function () {
    date_default_timezone_set('Asia/Bangkok');
    $createddate = Carbon::now()->toDateTimeString();
     $cenvertedTime = date('Y-m-d H:i:s',strtotime('+7 hour',strtotime($createddate)));
    return $createddate." to ".$cenvertedTime;
});

Route::get('/', function () {
    return redirect('https://rewards.rabbit.co.th/en');
});

Route::get('/update-time', 'SurveyController@updateTime');

Route::get('/extra-cash', 'SurveyController@ec');

Route::get('/personal-loan', 'SurveyController@pc');

Route::get('/thankyou', 'SurveyController@create');
Route::post('/thankyou', 'SurveyController@create');

Route::get('/exportCSV', 'SurveyController@exportCSV');
Route::post('/exportCSV', 'SurveyController@exportCSV');

Route::get('/exportExcel', 'SurveyController@exportExcel');
Route::post('/exportExcel', 'SurveyController@exportExcel');

Route::get('/customer-export', function () {
    return view('export');
});

Route::get('/thankyou-extra-cash', function () {
    return view('thankyou-extra-cash');
});
Route::get('/thankyou-personal-loan', function () {
    return view('thankyou-personal-loan');
});

Route::get('/CheckEmail', 'SurveyController@checkEmail');
Route::get('/CheckPhone', 'SurveyController@checkPhone');


