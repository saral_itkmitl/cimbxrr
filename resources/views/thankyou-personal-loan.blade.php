@extends('layouts.app')

@section('content')

<body style="background-color:black;">

<div class="container" style="background-color: #000;">
    <div class="row" >
       <div class="one column" style="height:1px;">&nbsp;</div>
       
        <div class="ten columns" style=" text-align:center; font-size: 1.4em; color:white; padding-top:20px; padding-bottom: 10px; overflow:hidden; padding:5%;">
        <h1 style="color:#ff8300; font-weight: 400;" >ขอบคุณค่ะ</h1><br>
        ขอบคุณสำหรับข้อมูลเบื้องต้นในการสมัครผลิตภัณฑ์&nbsp;
และบริการผ่านทางออนไลน์&nbsp;
ทางเจ้าหน้าที่จะทำการติดต่อกลับโดยเร็วที่สุด
        </div>
        
        <div class="one column" style="height:1px;">&nbsp;</div>
    </div>
    
</div>

<!-- FORM START HERE -->

    <div class="container">
        <div class="row">
            <div class="twelves columns" style="text-align: center; color:#999999;">©2019 Rabbit Rewards Co., Ltd. </div>
        </div>
    </div>              
                 
</body>

@endsection