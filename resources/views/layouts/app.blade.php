<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png"/>
<!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Rabbit Rewards - CIMB</title>
  <meta name="description" content="Designed by Rabbit Rewards Team!">
  <meta name="author" content="RabbitRewards Co., Ltd.">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRBWXR6');</script>
<!-- End Google Tag Manager -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-132695750-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-132695750-2', { 'optimize_id': 'GTM-TDFKZ2D'});
</script>
<!-- End  Google Analytics -->


<!-- Favicon
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/rabbit/xapple-touch-icon-57x57.png.pagespeed.ic.Kv83gfAkcy.webp">
<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/rabbit/xapple-touch-icon-60x60.png.pagespeed.ic.wPF2y-gMyX.webp">
<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/rabbit/xapple-touch-icon-72x72.png.pagespeed.ic.3aQDL3Wg7D.webp">
<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/rabbit/xapple-touch-icon-76x76.png.pagespeed.ic.5i7j7lfwsk.webp">
<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/rabbit/xapple-touch-icon-114x114.png.pagespeed.ic.lJGztyxuV9.webp">
<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/rabbit/xapple-touch-icon-120x120.png.pagespeed.ic.x78ZT_IWsx.webp">
<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/rabbit/xapple-touch-icon-144x144.png.pagespeed.ic.fEKJC-Sl7t.webp">
<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/rabbit/xapple-touch-icon-152x152.png.pagespeed.ic.AGY-o9Zeb9.webp">
<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/rabbit/xapple-touch-icon-180x180.png.pagespeed.ic.Cn7F5T_Fxs.webp">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-32x32.png.pagespeed.ic.b_qDo3k2Eq.webp" sizes="32x32">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xandroid-chrome-192x192.png.pagespeed.ic.JTL1IbgwT-.webp" sizes="192x192">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-96x96.png.pagespeed.ic.g5kxEtDa8A.webp" sizes="96x96">
<link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-16x16.png.pagespeed.ic.VZ99nloZzF.webp" sizes="16x16">
<link rel="manifest" href="/images/favicon/rabbit/manifest.json">

<!-- -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

  <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

  <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/ >
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/survey-custom.css">

 <script >
         $(document).ready(function() {
              // Date picker

      $('#datetimepicker1').datetimepicker(
                    
                    {   viewMode: 'days',
						format: 'YYYY-MM-DD',
//						maxDate: '2009/12/31',
//						minDate: '1938/01/01',
                        useCurrent: false,
  
                    })
                 $('#datetimepicker2').datetimepicker(
                    
                    {   viewMode: 'days',
						format: 'YYYY-MM-DD',
//						maxDate: '2009/12/31',
//						minDate: '1938/01/01',
                        useCurrent: false,
  
                    })
                    .on('dp.hide', function (e) {
                    $('#optionForm').bootstrapValidator('revalidateField', 'dob');
   
                  });

   
              });
             
              $(window).load(function(){
             
        $("#checkEmail").change(function(){ 
             document.getElementById("button-sub").disabled = false;
            $("#user-result").html('<img src="img/ajax-loader.gif" class="img-loader">');
            var email = $("#checkEmail").val();
            
            if(validateEmail(email)){
                check_email_ajax(email);
                
        
            }else if(email==""){
                $("#user-result").empty();
            }
            else{
                
                $("#user-result").html('กรุณากรอกอีเมลล์ให้ถูกต้อง');
                document.getElementById("user-result").style.color = "red";
                document.getElementById("noty").style.margin = "0px 0px 5px 5px";
            }
        }); 
        
            $("#checkPhone").change(function(){  
            $("#user-result2").html('<img src="img/ajax-loader.gif" class="img-loader">');
            var tel = $("#checkPhone").val();

            if(validatePhone(tel)){
                  check_phone_ajax(tel);
            
            }else if(tel==""){
                $("#user-result2").empty();
            }
            else{
                $("#user-result2").html('กรุณากรอกเบอร์โทรศัพท์ที่มีอยู่จริง');
                document.getElementById("user-result2").style.color = "red";
                document.getElementById("noty2").style.margin = "0px 0px 5px 5px";
            }

        }); 
            
            function check_email_ajax(email){
            $("#user-result").html('<img src="images/ajax-loader.gif">');
  
             $.ajax({
                    type:"get",
                    url:"{{ url('/CheckEmail') }}",
                    data:
                        {email:email,
                        _token: '{{csrf_token()}}'},
           
                    success:function(data){

                        if(data==0){
                            document.getElementById("button-sub").disabled = false;
                            $("#user-result").html('สามารถใช้อีเมลล์นี้ได้');
                            document.getElementById("user-result").style.color = "green";
                           document.getElementById("noty").style.margin = "0px 0px 5px 5px";

                        }else{
                             
                           $("#user-result").html('อีเมลล์นี้ถูกใช้ไปแล้ว');
 
                           document.getElementById("user-result").style.color = "red";
                           document.getElementById("noty").style.margin = "0px 0px 5px 5px";
                            
                        }
                    }
                 });

            }
                  
            function check_phone_ajax(tel){
            $("#user-result2").html('<img src="images/ajax-loader.gif">');
  
             $.ajax({
                    type:"get",
                    url:"{{ url('/CheckPhone') }}",
                    data:
                        {tel:tel,
                        _token: '{{csrf_token()}}'},
           
                    success:function(data){

                        if(data==0){
                           document.getElementById("button-sub").disabled = false;
                            $("#user-result2").html('สามารถใช้เบอร์โทรศัพท์นี้ได้');
                            document.getElementById("user-result2").style.color = "green";
                            document.getElementById("noty2").style.margin = "0px 0px 5px 5px";

                        }

                        else{
                             
                            $("#user-result2").html('เบอร์โทรศัพท์นี้ถูกใช้ไปแล้ว');
 
                           document.getElementById("user-result2").style.color = "red";
                            document.getElementById("noty2").style.margin = "0px 0px 5px 5px";
                            
                        }
                    }
                 });

            }
            
           function validatePhone(full_phone) {
                
                if(full_phone.length != 10){
                    return false;
                }
                var two_phone = full_phone.substr(0,2);
                var two_valid = '';
                
                var two_1 = /08/;
                var two_2 = /09/;
                var two_3 = /06/;
                var phone = full_phone.substr(2,10);
               
                var phone_1 = /88888888/;
                var phone_2 = /11111111/;
                var phone_3 = /22222222/;
                var phone_4 = /33333333/;
                var phone_5 = /44444444/;
                var phone_6 = /55555555/;
                var phone_7 = /66666666/;
                var phone_8 = /77777777/;
                var phone_9 = /88888888/;
                var phone_10 = /99999999/;
                var phone_11 = /12345678/;
                var phone_12 = /87654321/;
                var phone_13 = /00001111/;
                var phone_14 = /90000000/;
               
                if(two_1.test(String(two_phone).toLowerCase())){
                    two_valid = 'A';
                }else if(two_2.test(String(two_phone).toLowerCase())){
                    two_valid = 'B';
                }else if(two_3.test(String(two_phone).toLowerCase())){
                    two_valid = 'C';
                }else{
                    return false;
                }
                
                if(two_valid != ''){
           
               
                if(phone_1.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_2.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_3.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_4.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_5.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_6.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_7.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_8.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_9.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_10.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_11.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_12.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_13.test(String(phone).toLowerCase())){
                    return false;
                }else if(phone_14.test(String(phone).toLowerCase())){
                    return false;
                }else{
                    return true;
                }
                       
                   }
            }

           function validateEmail(email) {
                
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var re_2 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrotrewards)+(.)+$/;
            var re_3 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrot)+(.)+$/;
            var re_4 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(wadr)+(.)+$/;
            var re_5 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(wrad)+(.)+$/;
            var re_6 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carrct)+(.)+$/;
            var re_7 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(caootrews)+(.)+$/;   
            var re_8 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(crrot)+(.)+$/;
            var re_9 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(c)+(.)+(ward)+(.)+$/;
            var re_10 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(.)+(reward)+(.)+$/;
            var re_11 = /^(088)+(.)@(.)+(rabbit)+(.)+$/;  
            var re_12 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(noemail)+(.)+$/; 
            var re_13 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(carot)+(.)+$/;
            var re_14 = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@(getnada.com)+$/;
            
               
               
            if(re.test(String(email).toLowerCase())){
                if(re_14.test(String(email).toLowerCase())){
                      return false 
                }else if(re_2.test(String(email).toLowerCase())){
                      return false 
                }else if(re_3.test(String(email).toLowerCase())){
                      return false 
                }else if(re_4.test(String(email).toLowerCase())){
                      return false 
                }else if(re_5.test(String(email).toLowerCase())){
                      return false 
                }else if(re_6.test(String(email).toLowerCase())){
                      return false 
                }else if(re_7.test(String(email).toLowerCase())){
                      return false 
                }else if(re_8.test(String(email).toLowerCase())){
                      return false 
                }else if(re_9.test(String(email).toLowerCase())){
                      return false 
                }else if(re_10.test(String(email).toLowerCase())){
                      return false 
                }else if(re_11.test(String(email).toLowerCase())){
                      return false 
                }else if(re_12.test(String(email).toLowerCase())){
                      return false 
                }else if(re_13.test(String(email).toLowerCase())){
                      return false 
                }else{
                    return true;
                }
               
            }else{
                 return false;
              }
            
               
               
        }


 
           
});     
    
      function checkSubmit(){
//            alert("hi");
//        $.ajaxSetup({
//    headers: {
//        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//    }
//});
                   setTimeout(function() {
                      	
                    document.getElementById("button-sub").disabled = true;
    
    
                }, 100);
             
           var gender1 = document.getElementById("q-gender1").checked;
           var gender2 = document.getElementById("q-gender2").checked;
           var gender3 = document.getElementById("q-gender3").checked;
           var name = document.getElementById("q-name").value;
           var lastname = document.getElementById("q-lastname").value;
           var tel = document.getElementById("checkPhone").value;
           var email = document.getElementById("checkEmail").value;
           var occu1 = document.getElementById("q-occu1").checked;
           var occu2 = document.getElementById("q-occu2").checked;
           var occu3 = document.getElementById("q-occu3").checked;
           var salary = document.getElementById("q-salary").value;
           var exp = document.getElementById("q-exp").value;
           var x = document.getElementById("user-result").innerHTML;
           var y = document.getElementById("user-result2").innerHTML; 
                    if(gender1=='' && gender2=='' && gender3==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณาเลือกเพศของคุณ");
                        
                    }
                    else if(name==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณากรอกชื่อของคุณ");
                        
                    }
                    else if(lastname==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณากรอกนามสกุลของคุณ");
                        
                    }
                    else if(tel==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณากรอกเบอร์โทรศัพท์ของคุณ");

                    }else if(y=='กรุณากรอกเบอร์โทรศัพท์ที่มีอยู่จริง' || x=='เบอร์โทรศัพท์นี้ถูกใช้ไปแล้ว'){
                         document.getElementById("button-sub").disabled = true;
                        alert(y);
                    }
                    else if(email==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณากรอกอีเมลของคุณ");
                        
                    }
                    else if(x=='กรุณากรอกอีเมลล์ให้ถูกต้อง' || x=='อีเมลล์นี้ถูกใช้ไปแล้ว'){                 
                        document.getElementById("button-sub").disabled = true;
                        alert(x);
                    }
                    
                
                    else if(occu1=='' && occu2=='' && occu3==''){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณาเลือกอาชีพของคุณ");
                        
                    }else if(salary=='เงินเดือน*'){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณาเลือกเงินเดือนของคุณ");
                        
                    }else if(exp=='ระยะเวลาการทำงาน*'){                 
                        document.getElementById("button-sub").disabled = true;
                        alert("กรุณาเลือกระยะเวลาการทำงานของคุณ");
                        
                    }
                    

               setTimeout(function() {
                      
                      document.getElementById("button-sub").disabled = false;
                }, 5500);

            };
       
     


    
</script>
 
</head>

<body>

<div class="main-header" style="width:100%;">
    <div class="head-left">
        <img src="images/logo-rr.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 23px;">
    </div>
    <div class="head-right">
        <img src="images/logo-cimb.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 50px;">
    </div>  
    <div style="clear:both;"></div> 
</div>
    
          <main >
            @yield('content')
        </main>
    
</body>
</html>