@extends('layouts.app')

@section('content')


<!-- FORM START HERE -->
<form action="{{url('/exportExcel')}}" method="post">
    @csrf
<input type="hidden" name="campaign_type" value="extra-cash">
<div class="container">
    <div class="row" style="text-align: center;  background-color: white;padding:5%;">
        <div class="two columns" style="height:1px;"></div>
        <div class="four columns" style="background-color: white;  font-size:1.4em; color:#82bc00;">
             <input type="text" class="form-control" name="start" placeholder="Start date" id="datetimepicker1" autocomplete="off"   />
        </div>
        <div class="four columns" style="background-color: white;  font-size:1.4em; color:#82bc00;">
             <input  type="text" class="form-control" name="ended" placeholder="End date" id="datetimepicker2" autocomplete="off"   />
        </div>
        <div class="two columns" style="height:1px;"></div>
    </div>

    <div class="row" style="text-align:center; background-color: white; padding:5%;">
        <div class="two columns" style="height:1px;"></div>
        <div class="eight columns" style=""><input type="submit" name="submit" class="button-primary-green" value="Download"> </div>
        <div class="two columns" style="height:1px;"></div>
    </div>
</div>
</form>
    <div class="container">
        <div class="row">
            <div class="twelves columns" style="text-align: center; color:#999999;">©2019 Rabbit Rewards Co., Ltd. </div>
        </div>
    </div>              
                 

@endsection