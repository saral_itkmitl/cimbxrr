@extends('layouts.app')

@section('content')

<div class="container">
   <!--
    <div class="row" style="background-color: #ff8300;">
        <div class="two columns" style="height:1px;"></div>
        <div class="eight columns" style=" text-align:center; font-size: 1.4em; color:white; padding-top:10px; padding-bottom: 10px; padding-left:10px; padding-right:10px; overflow:hidden;">เคลียร์ให้หมด...ด้วยการรีไฟแนนซ์กับ <span style="font-weight: bold;">สินเชื่อบุคคล ธนาคาร ซีไอเอ็มบี ไทย</span>
        </div>
        <div class="two columns" style="height:1px;"></div>
    </div>
    -->
      
    <div class="row" class="" style="background-color: #000; ">
        <picture>
      <source srcset="images/pl-wide.jpg" media="(min-width: 1200px)" />
      <source srcset="images/pl-wide.jpg" media="(min-width: 1000px)" />
      <source srcset="images/pl-wide.jpg" media="(min-width: 750px)"  />
      <source srcset="images/pl-wide.jpg" media="(min-width: 550px)"   />
      <source srcset="images/pl-mobile.jpg" media="(min-width: 400px)" />
      <source srcset="images/pl-mobile.jpg" media="(min-width: 350px)" />
      <img src="images/pl-mobile.jpg" alt="example" class="u-max-full-width"  />
        </picture>
      </div> 
</div>

<!-- FORM START HERE -->
<form action="{{url('/thankyou')}}" method="post">
    @csrf
    <input type="hidden" name="campaign_type" value="personal-loan">
<div class="container">
    <div class="row" style="text-align: center;  background-color: white;">
        <div class="two columns" style="height:1px;"></div>
        <div class="eight columns" style="background-color: white; padding-top:10px;  font-size:1.4em; color:#ff8300;">
           @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>
            @endif
            กรุณากรอกข้อมูลของคุณ<br>แล้วเราจะรีบติดต่อกลับไปโดยเร็วที่สุด
        </div>
        <div class="two columns" style="height:1px;"></div>
    </div>
    <div class="row" style="text-align:left; background-color: white; padding:2%;">
        <div class="two columns" style="height:1px;"></div>
        <div class="eight columns" style="">    
                
            <label class="container-f ">นาย
              <input type="radio" id="q-gender1"  value="นาย" name="gender" required>
              <span class="checkmark"></span>
            </label>
            &nbsp;&nbsp;&nbsp;
            <label class="container-f ">นางสาว
              <input type="radio" id="q-gender2"  value="นางสาว" name="gender">
              <span class="checkmark"></span>
            </label>
            &nbsp;&nbsp;&nbsp;
            <label class="container-f ">นาง
              <input type="radio" id="q-gender3"  value="นาง" name="gender">
              <span class="checkmark"></span>
            </label><br>
            <input type="text" name="name" class="u-full-width" placeholder="ชื่อ*" title="ชื่อ" id="q-name"   required data-errormessage-value-missing="กรุณาระบุชื่อของคุณค่ะ" />
            <input type="text" name="surname" class="u-full-width" placeholder="นามสกุล*" title="สกุล" id="q-lastname"  required data-errormessage-value-missing="กรุณาระบุชื่อของคุณค่ะ" />
            <br>
            <input type="tel" id="checkPhone"  name="MobilePhone" class="u-full-width" 
                            onkeypress='return event.charCode >= 43 && event.charCode <= 57' 
                            pattern=".{10,10}" maxlength="10"  title="กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง" data-fv-stringlength-max="10" required placeholder="เบอร์โทรศัพท์มือถือ*" data-errormessage-value-missing="กรุณาระบุหมายเลขโทรศัพท์ที่ถูกต้องค่ะ" />
            <div id="noty2">
                 <span id="user-result2"></span>
            </div>
           

            
             <input type="email" id="checkEmail"  name="email" class="u-full-width" placeholder="อีเมล*" required >
            
            <div id="noty">
                 <span id="user-result"></span>
            </div>
            
            <br>
            อาชีพ* <br>
               
            

            
            <label class="container-f">ข้าราชการ
              <input type="radio" value="ข้าราชการ" id="q-occu1"  name="occupation" required data-errormessage-value-missing="กรุณาระบุอาชีพของคุณค่ะ">
              <span class="checkmark"></span>
            </label>
            &nbsp;&nbsp;&nbsp;   
            <label class="container-f">พนักงานบริษัท
              <input type="radio" value="พนักงานบริษัท" id="q-occu2" name="occupation">
              <span class="checkmark"></span>
            </label>
            &nbsp;&nbsp;&nbsp;   
            <label class="container-f">พนักงานรัฐวิสาหกิจ
              <input type="radio" value="พนักงานรัฐวิสาหกิจ" id="q-occu3" name="occupation">
              <span class="checkmark"></span>
            </label>
            <br>
            
            <select class="u-full-width" name="salary" id="q-salary" required >
                      <option  disabled selected>เงินเดือน*</option>
                      <option  value="ต่ำกว่า 30,000 บาท" required>ต่ำกว่า 30,000 บาท</option>
                      <option  value="30,000 - 49,999 บาท">30,000 - 49,999 บาท</option>
                      <option  value="50,000 - 99,999 บาท">50,000 - 99,999 บาท</option>
                      <option  value="มากกว่า 100,000 บาท">มากกว่า 100,000 บาท</option>
            </select>
            <select class="u-full-width" name="workyear" id="q-exp" required >
                      <option  disabled selected>ระยะเวลาการทำงาน*</option>
                      <option  value="น้อยกว่า 1 ปี" required>น้อยกว่า 1 ปี</option>
                      <option  value="1 - 2 ปี">1 - 2 ปี</option>
                      <option  value="2 - 5 ปี">2 - 5 ปี</option>
                      <option  value="5 - 10 ปี">5 - 10 ปี</option>
                      <option  value="มากกว่า 10 ปี">มากกว่า 10 ปี</option>
            </select><br>
            <label class="container-f" style="padding-left:30px; font-size:small;">ท่านต้องการได้รับการติดต่อทางโทรศัพท์จากธนาคาร ซีไอเอ็มบี ไทย จำกัด (มหาชน) ท่านตกลงและยินยอมให้บริษัทเปิดเผยข้อมูลของท่านที่ให้ไว้ในเว็บไวต์นี้ต่อธนาคาร ซีไอเอ็มบีไทย จำกัด (มหาชน) เพื่อนำเสนอและหรือประชาสัมพันธ์ผลิตภัณฑ์ของธนาคาร
              <input type="checkbox" value="True" name="accept_cimb" checked required>
              <span class="checkmark-c"></span>
            </label><br>
            <label class="container-f" style="padding-left:30px; font-size:small;">โดยการคลิกที่ปุ่มยืนยัน ท่านยอมรับการสมัครสมาชิก, ข้อกำหนดและเงื่อนไข, นโยบายปลอดภัย และรับอีเมจากแรบบิท รีวอร์ดส ท่านจะได้รับคะแนน 500 คะแนน เมื่อผ่านการอนุมัติจากธนาคาร
              <input type="checkbox" value="True" checked required >
              <span class="checkmark-c"></span>
            </label>
        </div>
        <div class="two columns" style="height:1px;"></div>
    </div>
    <div class="row" style="text-align:center; background-color: white; padding:2%;">
        <div class="two columns" style="height:1px;"></div>
        <div class="eight columns" style=""><input type="submit" onclick="checkSubmit()" id="button-sub" name="submit" class="button-primary" value="ยืนยัน"> </div>
        <div class="two columns" style="height:1px;"></div>
    </div>
</div>
</form>
    <div class="container">
        <div class="row">
            <div class="twelves columns" style="text-align: center; color:#999999;">©2019 Rabbit Rewards Co., Ltd. </div>
        </div>
    </div>              
                 
@endsection