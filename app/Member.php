<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
        protected $table = 'cimb_leadgen_2019';
    
    
        protected $fillable = [
            'name', 'email', 'password', 'createddate'
        ];
    
        public $timestamps = false;
    
        protected $primaryKey = 'RID';
    
}
