<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Carbon\Carbon;
use Response;
use App\Exports\LeadsExport;
use Maatwebsite\Excel\Facades\Excel;

class SurveyController extends Controller
{

    public function ec()
    {
        return view('extra-cash-ldp');
    }

    
    public function pc()
    {
        return view('personal-loan-ldp');
    }

    public function checkEmail(Request $request){
        if (Member::where('email', '=', $request->email)->exists()) {
            return '1';
        }else{
            return '0';
        } 
    }
    
    public function checkPhone(Request $request){
        if (Member::where('tel', '=', $request->tel)->exists()) {
            return '1';
        }else{
            return '0';
        } 
    }
    
    public function create(Request $request)
    {
        try{

            if (Member::where('email', '=', $request->email)->exists()) {
                return back()->with('message', 'อีเมลนี้ถูกใช้ไปแล้ว');
            } 

            if (Member::where('tel', '=', $request->MobilePhone)->exists()) {
                return back()->with('message', 'เบอร์โทรศัพท์นี้ถูกใช้ไปแล้ว');
            }

            if(empty($request->campaign_type)){
                return back()->with('message', 'เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง');
            }else if(empty($request->gender)){
                return back()->with('message', 'กรุณาเลือกเพศของคุณ');
            }else if(empty($request->name)){
                return back()->with('message', 'กรุณากรอกชื่อของคุณ');
            }else if(empty($request->surname)){
                return back()->with('message', 'กรุณากรอกนามสกุลของคุณ');
            }else if(empty($request->MobilePhone)){
                return back()->with('message', 'กรุณากรอกเบอร์โทรศัพท์ของคุณ');
            }else if(empty($request->email)){
                return back()->with('message', 'กรุณากรอกอีเมลของคุณ');
            }else if(empty($request->occupation)){
                return back()->with('message', 'กรุณาเลือกอาชีพของคุณ');
            }else if(empty($request->salary)){
                return back()->with('message', 'กรุณาเลือกเงินเดือนของคุณ');
            }else if(empty($request->workyear)){
                return back()->with('message', 'กรุณาเลือกระยะเวลาการทำงานของคุณ');
            } 

            $member = new Member;
            $member->campaign_type = $request->campaign_type;    
            $member->title = $request->gender;
            $member->name = $request->name;
            $member->surname = $request->surname;
            $member->tel = $request->MobilePhone;
            $member->email = $request->email;
            $member->occupation = $request->occupation;
            $member->salary = $request->salary;
            $member->work_exp = $request->workyear;
            $member->accept_cimb_condition = "True";
            $member->accept_rr_member = "True";
            date_default_timezone_set('Asia/Bangkok');
            $member->createddate = Carbon::now()->toDateTimeString();
            $member->save();

            if($request->campaign_type == 'extra-cash'){
                return redirect('/thankyou-extra-cash');
            }else if($request->campaign_type == 'personal-loan'){
                return redirect('/thankyou-personal-loan');
            }else
                return back();
        
            
         }catch (\Exception $e){
            return redirect('/');
         }
    }
    
    public function exportExcel(Request $request){
        
        $start = date($request->start.' 00:00:00');
        $ended = date($request->ended.' 23:59:59');
//        return $start.' + '.$ended;
        return (new LeadsExport)->forBetween($start,$ended)->download('CIMB_lead('.$start.' to '.$ended.').xlsx');
        
    }
    
    public function exportCSV()
    {
    $headers = array(
        "Content-type" => "text/csv",
        "Content-Disposition" => "attachment; filename=file.csv",
        "Pragma" => "no-cache",
        "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
        "Expires" => "0"
    );

    $member = Member::all();
    
    $columns = array('campaign_type', 'title', 'name', 'surname', 'tel', 'email', 'occupation', 'salary', 'work_exp', 'createddate');

    $callback = function() use ($member, $columns)
        
    {
        
        $file = fopen('php://output', 'w');
        
        fputcsv($file, $columns);
        $array = array_map("utf8_decode", $columns);
        foreach($member as $m) {
            fputcsv($file, array($m->campaign_type, $m->title, $m->name, $m->surname, $m->tel, $m->email, $m->occupation, $m->salary, $m->work_exp, $m->createddate));
        }
        fclose($file);
    };
        
    return Response::stream($callback, 200, $headers);
}

    
}
