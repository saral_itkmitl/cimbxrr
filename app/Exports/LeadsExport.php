<?php

namespace App\Exports;

//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use App\Member;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LeadsExport implements FromQuery, WithHeadings, ShouldAutoSize
{
    use Exportable;

    public function headings(): array
    {
        return [
            'campaign_type',
            'title',
            'name',
            'surname',
            'tel',
            'email',
            'occupation',
            'salary',
            'work_exp',
            'createddate',
          ];
    }
    
    public function forBetween($start, $ended)
    {
        $this->start = $start;
        $this->ended = $ended;
        
        return $this;
    }

    public function query()
    {
        
        return Member::select( 'campaign_type','title','name','surname','tel','email','occupation','salary','work_exp','createddate')->whereBetween('createddate', [$this->start, $this->ended]);
    }
}